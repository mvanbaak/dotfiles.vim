" Let Vundle do it's work
source ~/.vim/vimrc.vundle

" My own custom settings
source ~/.vim/vimrc.options
source ~/.vim/vimrc.formatting
source ~/.vim/vimrc.mappings
source ~/.vim/vimrc.interface
source ~/.vim/vimrc.functions
source ~/.vim/vimrc.autocmd
"== End own custom setings =="

"== Plugin options =="
source ~/.vim/vimrc.plugins.ale
source ~/.vim/vimrc.plugins.ansible
source ~/.vim/vimrc.plugins.fugitive
source ~/.vim/vimrc.plugins.gutentags
source ~/.vim/vimrc.plugins.lightline
source ~/.vim/vimrc.plugins.lightline_buffer
source ~/.vim/vimrc.plugins.php
source ~/.vim/vimrc.plugins.phpcomplete
source ~/.vim/vimrc.plugins.netrw
"== End plugin options =="
