syntax on                                      " enable syntax highlighting
colorscheme torte                              " I like the torte colors
set mouse=a                                    " enable mouse
set mousehide                                  " hide the mouse cursor while typing
set background=dark                            " assume dark background, always
set modeline                                   " enable parsing mode-lines
set modelines=5                                " in the first or last 5 lines
set ruler                                      " show ruler, only used when lightline is not loaded
set number                                     " show line numbers on the left
set laststatus=2                               " Show statusline even if only one window is displayed
set noshowmode                                 " lightline shows mode, dont show in normal status line
set title                                      " Set the title of xterm etc
set showcmd                                    " Show command in statusline
set showmatch                                  " auto identify matching brackets
set hlsearch                                   " Make search results red
set foldenable                                 " enable folds
set foldmethod=marker                          " fold on {{{ }}}
set wildmenu                                   " Display results in wild menu
set scrolloff=2                                " Keep 2 lines of context when scrolling
set sidescrolloff=2                            " Keep 2 chars of context when scrolling
set splitbelow                                 " Open new horizontal splits below current buffer
set splitright                                 " open new vertical split to the right
set showtabline=2                              " Always show tabline

if !has('gui_running')
    " terminal vim
    set t_Co=256                               " force colors
else
    " macvim/gvim
    set guioptions-=T                          " No toolbar in gui mode
    set guioptions-=r                          " No righthand scrollbar
    set guioptions-=R                          " No, really, never
    set guioptions-=l                          " No lefthand scrollbar
    set guioptions-=L                          " No, really, never
    if has('mac')
        set guifont=Monaco\ for\ Powerline:h10 " Set nice font ;-)
        set fuoptions=maxvert,maxhorz          " fullscreen is FULL SCREEN
    endif
endif
