My vim config
-------------

- plugins are handled by Vundle
- vimrc is broken up in seperate files based on functionality
- tested on OSX
- It's the vim setup I use for all my projects, both daytime job as MvBCoding
- I like it
- If you dont like it, I dont give a fuck. Either move on or fork it and change it


Clone this repository to a directory, let's say ~/.vim and run install script
    
```
#!sh

$ git clone https://bitbucket.org/mvanbaak/dotfiles.vim.git ~/.vim
$ ~/.vim/install.sh
```


If you want to setup manually:

```
#!sh

# Create a ~/.vimrc which sources ~/.vim/vimrc
$ echo "source ~/.vim/vimrc" > ~/.vimrc
# Get this repo with config files
$ git clone https://bitbucket.org/mvanbaak/dotfiles.vim.git ~/.vim
# Install Vundle
$ git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# Install plugins
$ vim +PluginInstall +qall
```

Have fun!